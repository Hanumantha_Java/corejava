import java.util.Scanner;
import java.util.stream.IntStream;

public class ReverseString {

	static StringBuilder stringBuffer;

	public static void main(String[] args) {
		stringBuffer=new StringBuilder();
		Scanner scanner = new Scanner(System.in);
		String str = scanner.nextLine();
		scanner.close();
		String strall[]=str.split(" ");
		
		System.out.println(strall.length);
		IntStream.range(0,strall.length).forEach(k ->
		{
			
		char[] chars=strall[k].toCharArray();
		char ch;
		for(int i=0,j=(chars.length-1);i<(chars.length)/2;i++,j--)
		{
			ch=chars[i];
			chars[i]=chars[j];
			chars[j]=ch;
		}
		stringBuffer.append(String.valueOf(chars)+" ");
});
		System.out.println(stringBuffer);
}
}
	