
public class BinarySearchApp {

	public static void main(String[] args) {

		int arr[] = { 1, 2, 4, 7, 19, 23, 60 };
		int startindex = 0;
		int endindex = arr.length - 1;
		int index = binaryserach(arr, startindex, endindex, 3);
		System.out.println(index);

	}

	private static int binaryserach(int[] arr, int startindex, int endindex, int i) {

		if (endindex >= startindex) {

			if (arr[(endindex + startindex) / 2] == i) {
				return (endindex + startindex) / 2;
			}

			if (i > arr[(endindex + startindex) / 2]) {
				int subindex = ((endindex + startindex) / 2) + 1;
				return binaryserach(arr, subindex, endindex, i);
			} else {
				int subindex = ((endindex + startindex) / 2) - 1;

				return binaryserach(arr, startindex, subindex, i);

			}
		}
		return -1;
	}

}
