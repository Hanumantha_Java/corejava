import java.util.Scanner;

public class SumArray {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		int str = scanner.nextInt();
		int sum=0;
		while(str!=0)
		{
			sum+=str%10;
			str=str/10;
		}
		System.out.println(sum);
}
}