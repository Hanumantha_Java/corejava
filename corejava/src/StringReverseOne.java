package com.hanuman;

import java.util.Scanner;

public class StringReverseOne {

	public static void main(String a[]) {

		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();

		scanner.close();
		input = input.replaceAll("\\s", "").replaceAll(",", "").replaceAll("[0-9]", "");
		char eachchar[] = input.toCharArray();
		int length = input.length();
		int i, j;
		char ch;
		for (i = 0, j = (length - 1); i < length / 2; i++, j--) {
			ch = eachchar[i];
			eachchar[i] = eachchar[j];
			eachchar[j] = ch;
		}
		System.out.println(String.valueOf(eachchar));

	}

}

