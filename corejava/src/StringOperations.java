import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class StringOperations {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		String str = scanner.nextLine().replaceAll("\\s", "");
		char chars[] = str.toCharArray();
		Map<Character, Integer> mapdetails = new HashMap<>();
		for (int i = 0; i < chars.length; i++) {
			if (mapdetails.containsKey(chars[i])) {
				int j = mapdetails.get(chars[i]);
				mapdetails.put(chars[i], ++j);
			} else {
				mapdetails.put(chars[i], 1);
			}
		}
		System.out.println(mapdetails);

		// find out first repeated char and non repeated char
		
		for (int i = 0; i < chars.length; i++) {
			if (mapdetails.get(chars[i]) > 1) {
				System.out.println("first repeated chars:" + chars[i]);
				System.out.println("count is:" + mapdetails.get(chars[i]));
				break;
			}
		}
		// find out first non repeated char

		for (int i = 0; i < chars.length; i++) {
			if (mapdetails.get(chars[i]) == 1) {
				System.out.println("first non repeated chars:" + chars[i]);
				System.out.println("count is:" + mapdetails.get(chars[i]));
				break;
			}
		}
		// find out last non repeated char
		for (int i = (chars.length - 1); i >= 0; i--) {
			if (mapdetails.get(chars[i]) == 1) {
				System.out.println("last non repeated chars:" + chars[i]);
				System.out.println("count is:" + mapdetails.get(chars[i]));
				break;
			}
		}
	}

}
