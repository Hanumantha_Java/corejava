import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

/**
 * Simple demo that uses java.util.Timer to schedule a task 
 * to execute once 5 seconds have passed.
 */

public class Reminder {
    Timer timer;

    public Reminder() {
        timer = new Timer(true);
        Calendar today = Calendar.getInstance();
     
      today.set(Calendar.SECOND, 3);
        timer.schedule(new RemindTask(),10000);
	}

    class RemindTask extends TimerTask {
        public void run() {
            System.out.println("Time's up!");
            timer.cancel(); //Terminate the timer thread
        }
    }

    public static void main(String args[]) {
        new Reminder();
        System.out.println("Task scheduled.");
    }
}